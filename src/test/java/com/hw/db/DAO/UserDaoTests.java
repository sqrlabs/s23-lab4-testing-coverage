package com.hw.db.DAO;

import com.hw.db.models.User;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDaoTests {
    public UserDaoTests() {
    }

    @Test
    @DisplayName("com.hw.db.DAO.UserDAO.Change() Full full MC/DC coverage")
    void ChangeFullFullMCDCCoverageTest() {
        List<String> qList = List.of(
                "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;", 
                "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;", 
                "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;", 
                "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;", 
                "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;", 
                "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;", 
                "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;");
        List<User> uList = List.of(new User("nick", "email@example.com", null, null), 
                new User("nick", null, "fullname", null), 
                new User("nick", null, null, "about"), 
                new User("nick", "email@example.com", "fullname", null), 
                new User("nick", "email@example.com", null, "about"), 
                new User("nick", null, "fullname", "about"), 
                new User("nick", "email@example.com", "fullname", "about"));

        for(int i = 0; i < qList.size(); ++i) {
            JdbcTemplate JdbcTemplate = Mockito.mock(JdbcTemplate.class);
            new UserDAO(JdbcTemplate);
            User u = uList.get(i);
            String q = qList.get(i);
            UserDAO.Change(u);
            Mockito.verify(JdbcTemplate).update(Mockito.eq(q), new Object[]{Optional.ofNullable(Mockito.anyVararg())});
        }

        JdbcTemplate JdbcTemplate = Mockito.mock(JdbcTemplate.class);
        new UserDAO(JdbcTemplate);
        User user = new User("zxc", null, null, null);
        UserDAO.Change(user);
        Mockito.verifyNoInteractions(new Object[]{JdbcTemplate});
    }
}
