package com.hw.db.DAO;

import com.hw.db.models.Post;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class PostDaoTests {
    public PostDaoTests() {
    }

    @Test
    @DisplayName("com.hw.db.DAO.PostDAO.setPost() full Basis path coverage")
    void testSetPost() {
        List<String> qList = List.of(
                "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;",
                "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;",
                "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;",
                "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
        );
        int pid = 1;
        List<Post> pList = List.of(
                new Post("some", null, "forum", null, null, null, true),
                new Post(null, null, "forum", "message", null, null, true),
                new Post(null, new Timestamp(1L), "forum", null, null, null, true),
                new Post("some", null, "forum", "message", null, null, true),
                new Post("some", new Timestamp(1L), "forum", null, null, null, true),
                new Post(null, new Timestamp(1L), "forum", "message", null, null, true),
                new Post("some", new Timestamp(1L), "forum", "message", null, null, true));

        for(int i = 0; i < qList.size(); ++i) {
            JdbcTemplate JdbcTemplate = Mockito.mock(JdbcTemplate.class);
            new PostDAO(JdbcTemplate);
            Post empty = new Post("author", new Timestamp(0L), "forum", "sample message", 0, 0, false);
            Mockito.when((Post)JdbcTemplate.queryForObject(
                    Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                    (RowMapper)Mockito.any(PostDAO.PostMapper.class),
                    new Object[]{Mockito.eq(pid)})).thenReturn(empty);
            Post p = pList.get(i);
            String q = qList.get(i);
            PostDAO.setPost(Integer.valueOf(pid), p);
            Mockito.verify(JdbcTemplate).update(Mockito.eq(q), new Object[]{Optional.ofNullable(Mockito.anyVararg())});
        }

    }
}
