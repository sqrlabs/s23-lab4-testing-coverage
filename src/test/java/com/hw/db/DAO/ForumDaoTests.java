package com.hw.db.DAO;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class ForumDaoTests {
    public ForumDaoTests() {
    }

    @Test
    @DisplayName("com.hw.db.DAO.ForumDAO.UserList() Full Branch Coverage Test")
    void UserListFullBranchCoverageTest() {
        List<String> qList = List.of(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;",
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;",
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;",
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;");
        List<Integer> limitList = new ArrayList();
        limitList.add(null);
        limitList.add(null);
        limitList.add(20);
        limitList.add(null);
        List<String> sinceList = new ArrayList();
        sinceList.add("01.01.2000");
        sinceList.add("01.01.2000");
        sinceList.add(null);
        sinceList.add(null);
        List<Boolean> descList = new ArrayList();
        descList.add(true);
        descList.add(false);
        descList.add(false);
        descList.add(null);

        for(int i = 0; i < qList.size(); ++i) {
            JdbcTemplate JdbcTemplate = Mockito.mock(JdbcTemplate.class);
            new ForumDAO(JdbcTemplate);
            String slug = "slug";
            Number limit = limitList.get(i);
            String since = sinceList.get(i);
            Boolean desc = descList.get(i);
            String q = qList.get(i);
            ForumDAO.UserList(slug, limit, since, desc);
            Mockito.verify(JdbcTemplate).query(Mockito.eq(q), Mockito.any(Object[].class), (RowMapper)Mockito.any(UserDAO.UserMapper.class));
        }

    }
}
