package com.hw.db.DAO;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class ThreadDaoTests {
    public ThreadDaoTests() {
    }

    @Test
    @DisplayName("com.hw.db.DAO.ThreadDAO.treeSort()")
    void testTreeSort() {
        List<String> qlist = List.of(
                "SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch;",
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC ;",
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;",
                "SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch LIMIT ? ;",
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;",
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;");
        List<Integer> limitList = new ArrayList();
        limitList.add(null);
        limitList.add(null);
        limitList.add(null);
        limitList.add(1);
        limitList.add(1);
        limitList.add(1);
        List<Integer> sinceList = new ArrayList();
        sinceList.add(null);
        sinceList.add(1);
        sinceList.add(1);
        sinceList.add(null);
        sinceList.add(1);
        sinceList.add(1);
        List<Boolean> descList = new ArrayList();
        descList.add(null);
        descList.add(true);
        descList.add(false);
        descList.add(null);
        descList.add(true);
        descList.add(false);

        for(int i = 0; i < qlist.size(); ++i) {
            Integer limit = limitList.get(i);
            Integer since = sinceList.get(i);
            Boolean desc = descList.get(i);
            String q = qlist.get(i);
            JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
            new ThreadDAO(jdbcTemplate);
            ThreadDAO.treeSort(1, limit, since, desc);
            Mockito.verify(jdbcTemplate).query(Mockito.eq(q), (RowMapper)Mockito.any(PostDAO.PostMapper.class), Mockito.anyVararg());
        }

    }
}
