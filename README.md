# Lab4 -- Testing coverage 


[![pipeline status](https://gitlab.com/sqrlabs/s23-lab4-testing-coverage/badges/master/pipeline.svg)](https://gitlab.com/sqrlabs/s23-lab4-testing-coverage/-/commits/master)

- Full branch coverage for `ForumDAO::UserList` method in [`TestForumDaoUserList.java`](src/test/java/com/hw/db/DAO/ForumDaoTests.java)
- Full MC/DC coverage for `UserDAO::Change` method in [`TestUserDaoChange.java`](src/test/java/com/hw/db/DAO/UserDaoTests.java)
- Full Basis Path coverage for `PostDAO::setPost` method in [`TestPostDaoSetPost.java`](src/test/java/com/hw/db/DAO/PostDaoTests.java)
- Full Statement coverage for `ThreadDAO::treeSort` method in [`TestThreadDaoTreeSort.java`](src/test/java/com/hw/db/DAO/ThreadDaoTests.java)